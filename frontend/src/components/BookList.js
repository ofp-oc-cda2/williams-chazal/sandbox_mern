import React, { Component } from 'react';
import Container from 'react-bootstrap/Container';
import axios from 'axios';
import { Link } from 'react-router-dom';
import BookCard from './BookCard';
import BtnBack from './BtnBack';

class ShowBookList extends Component {
    constructor (props) {
        super(props);
        this.state = {
            books: []
        };
    }
    
    componentDidMount() {
        axios
            .get('http://localhost:8080/books')
            .then(res => {
                this.setState({
                    books: res.data
                })
            })
            .catch(err =>{
                console.log("Erreur lors de l'affichage de la bibliothèque" +err)
            })
    };

    render(){
        const books = this.state.books;
        console.log("PrintBook" + books);
        let bookList;

        if(!books) {
            bookList = "Il n'y pas de livre dans la bibliothèque"
        } else {
            bookList = books.map((book, k) => 
                <BookCard book={book} key={k} />
            )
        }
        return(
            <div className="ShowBookList">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <br />
                            <h2 className="display-4 text-center">Liste des livres</h2>
                        </div>

                        <div className="col-md-11">
                            <Link to="AddBook" className='btn btn-outline-warning float-left'>  Ajoutez un livre
                            </ Link>
                            <br />
                            <br />
                            <hr />
                        </div>

                    </div>

                    <div className="list">
                        {bookList}
                    </div>


                </div>
            </div>
        )
    }

    
}
export default ShowBookList;