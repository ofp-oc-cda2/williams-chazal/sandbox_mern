import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import withParams from './WithParams';
import Navigation from './Navigation';

class UpdateBook extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            author:'',
            category:'',
            descritpion:'',
            publishDate:''
        };
    }

    componentDidMount() {
        axios
            .get('http://localhost:8080/books/'+this.props.params.id)
            .then(res => {
                this.setState({
                    title: res.data.title,
                    author: res.data.author,
                    category: res.data.category,
                    descritpion: res.data.descritpion,
                    publishDate: res.data.publishDate
                })
            })
            .catch(err => {
                console.log("Erreur lors de la modification des informations")
            })
    };

    onChange = e => {
        this.setState({ [e.target.name]: e.target.value});
    };

    onSubmit = e => {
        e.preventDefault();

        const data = {
            title: this.state.title,
            author: this.state.author,
            category: this.state.category,
            descritpion: this.state.descritpion,
            publishDate: this.state.publishDate
        };

        axios
            .put('http://localhost:8080/books/'+this.props.params.id, data)
            .then(res => {
                this.props.history.push('/showBook/' +this.props.params.id);
            })
            .catch(err => {
                console.log("Erreur lors de la mise à jour des informations")
            })
    };

    render(){
        return (
            <div className="UpdateBookInfo">
              <div className="container">
                <div className="row">
                  <div className="col-md-8 m-auto">
                    <br />
                    <Link to="/" className="btn btn-outline-warning float-left">
                        Show BooK List
                    </Link>
                  </div>
                  <div className="col-md-8 m-auto">
                    <h1 className="display-4 text-center">Edit Book</h1>
                    <p className="lead text-center">
                        Update Book's Info
                    </p>
                  </div>
                </div>
      
                <div className="col-md-8 m-auto">
                <form noValidate onSubmit={this.onSubmit}>
                  <div className='form-group'>
                    <label htmlFor="title">Title</label>
                    <input
                      type='text'
                      placeholder='Title of the Book'
                      name='title'
                      className='form-control'
                      value={this.state.title}
                      onChange={this.onChange}
                    />
                  </div>
                  <br />
      
                  <div className='form-group'>
                  <label htmlFor="isbn">ISBN</label>
                    <input
                      type='text'
                      placeholder='ISBN'
                      name='isbn'
                      className='form-control'
                      value={this.state.isbn}
                      onChange={this.onChange}
                    />
                  </div>
      
                  <div className='form-group'>
                  <label htmlFor="author">Author</label>
                    <input
                      type='text'
                      placeholder='Author'
                      name='author'
                      className='form-control'
                      value={this.state.author}
                      onChange={this.onChange}
                    />
                  </div>
      
                  <div className='form-group'>
                  <label htmlFor="description">Description</label>
                    <input
                      type='text'
                      placeholder='Describe this book'
                      name='description'
                      className='form-control'
                      value={this.state.description}
                      onChange={this.onChange}
                    />
                  </div>
      
                  <div className='form-group'>
                  <label htmlFor="published_date">Published Date</label>
                    <input
                      type='date'
                      placeholder='published_date'
                      name='published_date'
                      className='form-control'
                      value={this.state.published_date}
                      onChange={this.onChange}
                    />
                  </div>
                  <div className='form-group'>
                  <label htmlFor="publisher">Publisher</label>
                    <input
                      type='text'
                      placeholder='Publisher of this Book'
                      name='publisher'
                      className='form-control'
                      value={this.state.publisher}
                      onChange={this.onChange}
                    />
                  </div>
      
                  <button type="submit" className="btn btn-outline-info btn-lg btn-block">Update Book</button>
                  </form>
                </div>
      
              </div>
            </div>
          );
        }
    
}

export default withParams (UpdateBook);