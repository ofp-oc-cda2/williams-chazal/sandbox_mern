import React, { Component } from 'react';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import axios from 'axios';
import BtnBack from './BtnBack';
/* import { Link } from 'react-router-dom'; */




class CreateBook extends Component {
    constructor() {
        super ();
        this.state = {
            title: '',
            author:'',
            category:'',
            descritpion:'',
            publishDate:''
        };
    }

    onChange = e => {
        this.setState({ [e.target.name] : e.target.value});
    };

    onSubmit = e => {
        e.preventDefault();

        const data = {
            title: this.state.title,
            author: this.state.author,
            category: this.state.category,
            descritpion: this.state.descritpion,
            publishDate: this.state.publishDate
        };

        axios
            .post('http://localhost:8080/books', data)
                
            .then(res => {
                this.setState({
                    title: '',
                    author:'',
                    category:'',
                    descritpion:'',
                    publishDate:''
                })
                console.log('Livre Ajouté')
                alert ("Livre ajouté à la bibliothèque")
            
            })
            .catch(err => {
                
                console.log("Erreur lors  de l'ajout du Livre" + err)
                alert("Erreur lors de l'ajout du livre "  +err);
            })
                
    };

    render(){
        return (
            <div>
                <Container className='d-grid'>    
                    <Form noValidate onSubmit={this.onSubmit} className='text-center'>
                        <img 
                            className='logo mb-4'
                            src="./logo192.png" alt="Logo react" />
                       <h2>Ajoutez un livre à votre bibliothèque</h2>
                       <BtnBack/>
                       <Form.Group>
    
                           <Form.Control 
                                type="text" 
                                size="lg" 
                                placeholder='Titre du livre' 
                                className='mb-4' 
                                name='title'
                                value={this.state.title}
                                onChange={this.onChange}
                            />
    
                           <Form.Control 
                                type="text" 
                                size="lg" 
                                placeholder="Nom de l'auteur" 
                                className='mb-4'
                                name='author'
                                value={this.state.author}
                                onChange={this.onChange}
                            />
    
                           <Form.Control 
                                type="text" 
                                size="lg" 
                                placeholder="Genre du livre" 
                                className='mb-4'
                                name='category'
                                value={this.state.category}
                                onChange={this.onChange}
                            />  
                            
    
                            <Form.Control 
                                as="textarea" 
                                rows={5} 
                                placeholder='Description du livre' 
                                className='mb-4'
                                name='descritpion'
                                value={this.state.descritpion}
                                onChange={this.onChange}

                            />
                          
                           <Form.Control 
                                type="text" 
                                size="lg" 
                                placeholder="Année de publication" 
                                className='mb-4'
                                name='publishDate'
                                value={this.state.publishDate}
                                onChange={this.onChange}
                            />
    
                       </Form.Group>
                    
                        <Button 
                            type='submit'
                            size='lg'>Ajouter le livre 
                        </Button>
    
                       
                    </Form>
                </Container>
    
            </div>

        );
    }

}





export default CreateBook;

 