import React from 'react';
import { NavLink } from 'react-router-dom';


const Navigation = () => {
    return (
        <div className="navigation">
            <ul>
                <NavLink to="/" className={(nav) => (nav.isActive ? "nav-active" : "")}>
                    <li>Bibliothèque</li>
                </NavLink>
                <NavLink to="/Addbook" className={(nav) => (nav.isActive ? "nav-active" : "")}>
                    <li>Ajouter un livre</li>
                </NavLink>
            </ul>
        </div>
    );
};

export default Navigation;