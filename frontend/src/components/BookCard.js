import React from 'react';
import { Link } from 'react-router-dom'


const BookCard = (props) => {
        const book = props.book;
    return (
        <div className="card-container">
            <img src="https://d827xgdhgqbnd.cloudfront.net/wp-content/uploads/2016/04/09121712/book-cover-placeholder.png" alt="" className='mb-3' />
            <div className="desc">
                <h2>
                    <Link to={`/showBook/${book._id}`}>
                        { book.title }
                    </Link>
                </h2>
                <h3>{book.author}</h3>
                <p>{book.category}</p>
            </div>
        </div>
    );
};

export default BookCard;