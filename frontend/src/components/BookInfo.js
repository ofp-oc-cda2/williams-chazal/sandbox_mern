import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import withParams from './WithParams';
import Navigation from './Navigation';



class showBookDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      book: {}
    };
  }

  componentDidMount() {
    console.log("Print id: " , this.props);
    axios
      .get('http://localhost:8080/books/'+this.props.params.id)
      .then(res => {
        // console.log("Print-showBookDetails-API-response: " + res.data);
        this.setState({
          book: res.data
        })
      })
      .catch(err => {
        console.log("Error from ShowBookDetails");
      })
  };

  onDeleteClick (id) {
    axios
      .delete('http://localhost:8080/books/'+id)
      .then(res => {
        this.props.history.push("/");
          console.log('Livre supprimé')
      })
      .catch(err => {
        console.log("Error fromm ShowBookDetails_deleteClick");
      })
  };


  render() {

    const book = this.state.book;
    let bookTitle = book.title 
    let BookItem = <div>
      <table className="table table-hover table-dark">
        {/* <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">First</th>
            <th scope="col">Last</th>
            <th scope="col">Handle</th>
          </tr>
        </thead> */}
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>Title</td>
            <td>{ book.title }</td>
          </tr>
          <tr>
            <th scope="row">2</th>
            <td>Auteur</td>
            <td>{ book.author }</td>
          </tr>
          <tr>
            <th scope="row">3</th>
            <td>Catégorie</td>
            <td>{ book.category }</td>
          </tr>
          <tr>
            <th scope="row">4</th>
            <td>Résumé</td>
            <td>{ book.descritpion}</td> 
          </tr>
          <tr>
            <th scope="row">5</th>
            <td>Date de publication</td>
            <td>{ book.publishDate }</td>
          </tr>
          
        </tbody>
      </table>
    </div>

    return (
      
      <div className="ShowBookDetails">
        <Navigation />
        <div className="container">
          <div className="row">
            <div className="col-md-10 m-auto">
              <br /> <br />
              <Link to="/" className="btn btn-outline-warning float-left">
                  Voir la bibliothèque
              </Link>
            </div>
            <br />
            <div className="col-md-8 m-auto">
              <h1 className="display-4 text-center">{ bookTitle }</h1>
              <p className="lead text-center">
                  View Book's Info
              </p>
              <hr /> <br />
            </div>
          </div>
          <div>
            { BookItem }
          </div>

          <div className="row">
            <div className="col-md-6">
              <button type="button" className="btn btn-outline-danger btn-lg btn-block" onClick={this.onDeleteClick.bind(this,book._id)}>Supprimer le livre</button><br />
            </div>

            <div className="col-md-6">
              <Link to={`/updateBook/${book._id}`} className="btn btn-outline-info btn-lg btn-block">
                    Modifier les informations
              </Link>
              <br />
            </div>

          </div>
            {/* <br />
            <button type="button" class="btn btn-outline-info btn-lg btn-block">Edit Book</button>
            <button type="button" class="btn btn-outline-danger btn-lg btn-block">Delete Book</button> */}

        </div>
      </div>
    );
  }
}

export default withParams(showBookDetails);