import React from 'react';
import { Link } from 'react-router-dom';
import Button from 'react-bootstrap/Button';

const BtnBack = () => {
    return (
        <div>
            
            <Link to="/" className="test btn float-left mb-4">
                  Voir la Bibliothèque
            </Link>
        </div>
    );
};

export default BtnBack;