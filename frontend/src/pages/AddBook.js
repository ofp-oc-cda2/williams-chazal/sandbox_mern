import React from 'react';
import Navigation from '../components/Navigation';
import CreateBook from '../components/CreateBook';


const About = () => {
    return (
        <div>
           <Navigation />

            <CreateBook />
        </div>
    );
};

export default About;