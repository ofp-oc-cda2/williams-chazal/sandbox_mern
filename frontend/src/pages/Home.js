import React from 'react';
import Container from 'react-bootstrap/Container';
import Navigation from '../components/Navigation';
import BookList from '../components/BookList';

const Home = () => {
    return (
        <div>
            <Navigation/>
            <Container className='text-center'>
                <h1>Bibliothèque</h1> 
            <BookList/>
            </Container>
           
        </div>
    );
};

export default Home;