import React from 'react';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import About from './pages/AddBook';
import Home from './pages/Home';
import BookInfo from './components/BookInfo';
import UpdateBook from './components/UpdateBook';






const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/Addbook" element={<About />} />
        <Route path='/showBook/:id' element={<BookInfo/>} />
        <Route path='/updateBook/:id' element={<UpdateBook/>} />
      </Routes>
    
    </BrowserRouter>
  );
};



export default App;
