const mongoose = require('mongoose');

mongoose.connect(
    "mongodb://localhost:27017/sandboxMern",
    {
        useNewUrlParser: true, useUnifiedTopology: true
    },
    (err) => {
        if (!err) console.log ("Connexion en Base de données Réussis");
        else console.log("Erreur de Connexion : " + err)
    }
)