const mongoose = require('mongoose');

const bookModel = mongoose.model(
    "sandboxMern",
    {
        title: {
            type: String,
            required: true
        },
        author: {
            type: String,
            required: true
        },
        descritpion: {
            type: String
        },
        category: {
            type: String
        },
        publishDate: {
            type: String
        }
    },
    "books"
)

module.exports = { bookModel };