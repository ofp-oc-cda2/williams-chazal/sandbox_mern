const express = require ('express')
const router = express.Router();
const ObjectID = require('mongoose').Types.ObjectId


const { bookModel } = require('../models/bookModel');

router.get('/', (req, res) =>{
    if (req.query.title){
        bookModel.find(
            {title : req.query.title},
            (err, docs) => {
                if (!err) res.send(docs);
                else console.log("Update error : " + err);
            }
        )
    } else if (req.query.author){
        bookModel.find(
            {author : req.query.author},
            (err, docs) => {
                if (!err) res.send(docs);
                else console.log("Update error : " + err);
            }
        )
    } else {
        bookModel.find((err, docs) =>{
            if (!err) res.send(docs)
            else console.log('Error getting data :' + err);
        })
    }
    
})

// AJout d'un nouveau livre

router.post('/', (req, res) => {
    const newBook = new bookModel({
        title: req.body.title,
        author: req.body.author,
        descritpion: req.body.descritpion,
        category: req.body.category,
        publishDate: req.body.publishDate
    });

    newBook.save((err, docs) => {
        if (!err) res.send(docs)
        else console.log ('Error Creating new books: ' + err);
    })
})



// Modifier un post

router.put("/:id", (req, res) => {
    if (!ObjectID.isValid(req.params.id))
    return res.status(400).send (" ID unknow : " + req.params.id)

    const updateBook = {
        title: req.body.title,
        author: req.body.author,
        descritpion: req.body.descritpion,
        category: req.body.category,
        publishDate: req.body.publishDate
    };

    bookModel.findByIdAndUpdate(
        req.params.id,
        { $set: updateBook },
        { new: true },
        (err, docs) => {
            if(!err) res.send(docs);
            else console.log("Update error : " + err);
        }
    )

})

// Supprimer un post

router.delete("/:id", (req, res) => {
    if (!ObjectID.isValid(req.params.id))
    return res.status(400).send('ID Unknow :' +req.params.id)

    bookModel.findByIdAndDelete(
        req.params.id,
        (err, docs) => {
            if (!err) res.send(docs);
            else console.log("Delete Error: " +err)
        }
    )
})


// trouver un post par son id
router.get('/:id', (req, res) => {
    if (!ObjectID.isValid(req.params.id))
    return res.status(400).send("ID unknow : " + req.params.id)
    


    bookModel.findById(
        req.params.id,
        (err, docs) => {
            if (!err) res.send(docs);
            else console.log("Update error : " + err);
        }
    )
});

// trouver un livre par son nom








module.exports = router;