const mongoose = require('mongoose')
const express = require('express')
const cors = require('cors')
const app = express();


require('./models/dbConfig');

const booksRoutes = require('./routes/booksController');
const bodyParser = require('body-parser');

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server connected to port ${PORT}`);
});

app.use(cors());
app.use(bodyParser.json());
app.use('/books', booksRoutes);

app.get("/", (req, res) => {
    res.json({ message: "Bienvenue Camarade." });
  });